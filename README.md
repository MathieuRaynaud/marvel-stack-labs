# Marvel API Client

[[_TOC_]]

## Run the application in a development environment

To run the application in a development environment, please follow these instructions:
1. Clone the project to a local repository: 
```bash
git clone https://gitlab.com/MathieuRaynaud/marvel-stack-labs.git
```
2. Go to the created directory:
```bash
cd marvel-stack-labs
```
3. Install the required dependencies:
```bash
yarn install
```
4. Run the application:
```bash
yarn dev
```

## Deploy your Docker container

To build the project's Docker image, please execute the following line in a terminal:
```bash
docker build . -t marvel-api-client-image
```

To run a container using the image, please execut the following line in a terminal:
```bash
docker run -d -p 3000:3000 -e API_PUBLIC_KEY=<YOUR_API_PUBLIC_KEY> -e API_PRIVATE_KEY=<YOUR_API_PRIVATE_KEY> --name marvel-api-client marvel-api-client-image
```

## Get the API documentation

To have a look to the API documentation, please connect to the http://localhost:3000/api-docs/ URL and see the Swagger documentation.

## Launch tests

### Domain tests

Domain tests can be launched by executing the following line in a terminal: 
```bash
yarn test-domain
```

### Infra tests

Infra tests can be launched by executing the following line in a terminal: 
```bash
yarn test-infra
```

### Integration tests

Integration tests can be launched by executing the following line in a terminal: 
```bash
yarn test-integration
```