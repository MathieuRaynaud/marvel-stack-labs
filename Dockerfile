## Stage 0: Application building
FROM node:16 AS build

WORKDIR /usr

COPY package.json ./
COPY tsconfig.json ./
COPY src ./src

RUN yarn install
RUN yarn build

COPY src/config/swagger.json ./dist/config/

## Stage 1: Application run
FROM node:16 AS run

WORKDIR /app/marvel-api-client

COPY package.json ./

ENV NODE_ENV=production

COPY --from=build /usr/node_modules ./node_modules
COPY --from=build /usr/dist ./dist

EXPOSE 3000
CMD ["yarn", "start"]