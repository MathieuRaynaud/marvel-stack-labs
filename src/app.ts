const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./config/swagger.json');
import express from 'express';
import MarvelCharacterService from './domain/service/MarvelCharacterService';
import errorHandler from './errors/ErrorHandler';
import MarvelAPIClient from './infra/MarvelAPIClient';
import MarvelController from './interfaces/MarvelController';
const config = require('./config/default.json');

// Init the MarvelCharacterService to use in the app
const initService = (): MarvelCharacterService => {
    const apiBaseUrl = config.marvelAPI.apiBaseUrl;

    if (!process.env.API_PUBLIC_KEY || !process.env.API_PRIVATE_KEY){
        throw new Error("API keys are not defined in the execution environment");
    }

    const provider = new MarvelAPIClient(apiBaseUrl, process.env.API_PUBLIC_KEY, process.env.API_PRIVATE_KEY);

    const service = new MarvelCharacterService(provider);

    return service
}

const service = initService();

const controller = new MarvelController(service);

const app = express();
const port = process.env.PORT || 3000;

// Swagger
app.use('/api-docs', swaggerUi.serve);
app.get('/api-docs', swaggerUi.setup(swaggerDocument));

// GET /character route
app.get('/character', controller.getCharacterByName);

// Errors handler
app.use(errorHandler);

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});

export default app;