import { NextFunction, Request, Response } from 'express';
import MarvelCharacterService from '../domain/service/MarvelCharacterService';
import ErrorMessages from '../errors/ErrorMessages';

class MarvelController {

    service: MarvelCharacterService;

    constructor(service: MarvelCharacterService) {
        this.service = service;
    }
    
    getCharacterByName = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const name = req.query.name as string;
    
            if (!name){
                throw new Error(`${ErrorMessages.QUERY_PARAMETER_MANDATORY} name`)
            }
    
            const character = await this.service.findCharacterByName(name);
    
            if (!character) {
                throw new Error(ErrorMessages.NO_RESULTS_FOUND)
            }
    
            const comics = await this.service.findComicsByCharacterId(character.id);
    
            res.status(200);
            res.json({...character, comics});
        } catch (err) {
            next(err)
        }
    }
}
 
export default MarvelController;