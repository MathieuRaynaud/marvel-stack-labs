import Character from "../model/Character";
import Comic from "../model/Comic";

interface MarvelProvider {
    findCharacterByName (name: string) : Promise<Character|undefined>;
    findComicsByCharacterId (id: number) : Promise<Comic[]>;
}

export default MarvelProvider;