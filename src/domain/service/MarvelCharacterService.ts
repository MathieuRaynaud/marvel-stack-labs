import Character from "../model/Character";
import Comic from "../model/Comic";
import MarvelProvider from "../provider/MarvelProvider";

class MarvelCharacterService {

    // The Marvel Provider
    marvelProvider: MarvelProvider;

    constructor(marvelProvider: MarvelProvider) {
        this.marvelProvider = marvelProvider;
    }

    async findCharacterByName(name: string) : Promise<Character|undefined> {
        return this.marvelProvider.findCharacterByName(name);
    }

    async findComicsByCharacterId(characterId: number) : Promise<Comic[]> {
        return this.marvelProvider.findComicsByCharacterId(characterId);
    }
    
}

export default MarvelCharacterService;