import Comic from "./Comic";

class Character {

    // The character id
    id: number

    // The character name
    name: string;
    
    // The character description
    description: string;


    // The constructor;
    constructor(id: number, name: string, description: string) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

}

export default Character;