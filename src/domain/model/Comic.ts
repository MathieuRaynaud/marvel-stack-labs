class Comic {

    // The Comic title
    title: string;

    // The Comic description
    description: string;

    // The constructor
    constructor(title: string, description: string) {
        this.title = title;
        this.description = description;
    }

}

export default Comic;