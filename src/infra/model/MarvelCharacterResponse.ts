
import Character from "../../domain/model/Character";

class MarvelCharacterResponse {

    // The Marvel character response
    data:{
        // The number of returned results
        total: number;

        // The results
        results: {
            // The character's ID
            id: number;

            // The character's name
            name: string;

            // The character's description
            description: string;
        }[]
    }

    constructor (object: MarvelCharacterResponse) {
        this.data = object.data
    }
}

export default MarvelCharacterResponse;