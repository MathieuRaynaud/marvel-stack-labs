class MarvelComicResponse {

    // The Marvel Comic response
    data: {
        // The number of returned results
        total: number,

        // The results
        results: {

            // The result's title
            title: string,

            // The result's description
            description: string
        }[]
    }

    // The constructor
    constructor(object: MarvelComicResponse) {
        this.data = object.data;
    }

}

export default MarvelComicResponse;