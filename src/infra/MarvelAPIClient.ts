import axios, { AxiosInstance } from "axios";
import Character from "../domain/model/Character";
import Comic from "../domain/model/Comic";
import MarvelProvider from "../domain/provider/MarvelProvider";
import ErrorMessages from "../errors/ErrorMessages";
import MarvelCharacterResponse from "./model/MarvelCharacterResponse";
import MarvelComicResponse from "./model/MarvelComicResponse";
const crypto = require('crypto');

class MarvelAPIClient implements MarvelProvider {
    // Marvel API base URL
    apiBaseUrl: string;

    // Marvel API public key
    APIPublicKey: string|undefined;

    // Marvel API private key
    APIPrivateKey: string|undefined;

    // Axios instance
    instance: AxiosInstance;

    constructor(apiBaseUrl: string, apiPublicKey: string, apiPrivateKey: string) {
        this.apiBaseUrl = apiBaseUrl;
        this.APIPublicKey = apiPublicKey;
        this.APIPrivateKey = apiPrivateKey;

        this.instance = axios.create({
            baseURL: `${this.apiBaseUrl}/v1/public`,
            params: {
                'apikey': this.APIPublicKey
            },
        })

        this.instance.interceptors.request.use((config) => {
            let tsHash = this.computeHash();
            config.params.ts = tsHash.ts;
            config.params.hash = tsHash.hash;
            return config;
        });
    }

    
    computeHash () :{ts: number, hash: string} {
        let timestamp = Date.now();
        let string = `${timestamp}${this.APIPrivateKey}${this.APIPublicKey}`

        return {ts: timestamp, hash:crypto.createHash('md5').update(string).digest('hex')}
    }

    async findCharacterByName (name: string) : Promise<Character|undefined> {
        let {data, status} = await this.instance.get<MarvelCharacterResponse>(
            `/characters?name=${name}`
        );

        if (status != 200) {
            throw new Error(`${ErrorMessages.REQUEST_FAILED_WITH_STATUS_CODE} ${status}`);
        }
        else if (data.data.total == 0) {
            throw new Error(`${ErrorMessages.NO_RESULTS_FOUND}`)
        }

        return new Character(data.data.results[0].id, data.data.results[0].name, data.data.results[0].description);
    }

    async findComicsByCharacterId (characterId: number) : Promise<Comic[]> {
        let response = await this.instance.get<MarvelComicResponse>(
            `/comics?characters=${characterId}`
        )
        
        return response.data.data.results.map((comicResponse) => {
            return new Comic(comicResponse.title, comicResponse.description)
        })
    }
}

export default MarvelAPIClient;