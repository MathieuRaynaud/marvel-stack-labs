enum ErrorMessages {
    NO_RESULTS_FOUND="No results found",
    REQUEST_FAILED_WITH_STATUS_CODE="Request failed with status code",
    QUERY_PARAMETER_MANDATORY="The following query parameter is mandatory:"
}

export default ErrorMessages;