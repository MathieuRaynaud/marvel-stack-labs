import { NextFunction, Request, Response } from 'express';

function errorHandler (err: Error, req: Request, res: Response, next: NextFunction): void {

    if (res.headersSent) {
        return next(err)
    }

    console.error(err.stack);
    res.status(500).send(err.message);
}

export default errorHandler;