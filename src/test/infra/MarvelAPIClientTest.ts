import { assert, expect } from "chai";
import MarvelAPIClient from "../../infra/MarvelAPIClient";
import config from '../../config/default.json';
import Character from "../../domain/model/Character";
import Comic from "../../domain/model/Comic";
import ErrorMessages from "../../errors/ErrorMessages";

// Character model test class
describe("Marvel API Client Test", () => {

    if (!process.env.API_PUBLIC_KEY || !process.env.API_PRIVATE_KEY){
        throw new Error("API keys are not set in the execution environment")
    }

    const client = new MarvelAPIClient(config.marvelAPI.apiBaseUrl, process.env.API_PUBLIC_KEY, process.env.API_PRIVATE_KEY);

    describe( "Get the character Hulk", () =>  {

        var character: Character;

        before(async () => {
            let response = await client.findCharacterByName("Hulk");

            if (!response){
                throw new Error("response is undefined");
            }

            character = response;
        })

        it("Is Hulk existing",() => {
            assert.isOk(character, "Hulk exists")
        });

        it("Is Hulk having the name Hulk",() => {
            assert.equal(character.name, "Hulk")
        });

        it("Is Hulk having a description",() => {
            assert.isOk(character.description, "Hulk has a description")
        });
    });

    describe( "Get the character Iron Man", () =>  {

        var character: Character;

        before(async () => {            
            let response = await client.findCharacterByName("Iron Man");

            if (!response){
                throw new Error("response is undefined");
            }

            character = response;
        })

        it("Is Iron Man existing",() => {
            assert.isOk(character, "Iron Man exists")
        });

        it("Is Iron Man having the name Iron Man",() => {
            assert.equal(character.name, "Iron Man")
        });

        it("Is Iron Man having a description",() => {
            assert.isOk(character.description, "Iron Man has a description")
        });
    });

    describe("Get the comics in which character 1009187 appears", () => {

        var comics: Comic[];

        before(async () => {
            let response = await client.findComicsByCharacterId(1009187);

            if (!response) {
                throw new Error("response is undefined")
            }

            comics = response;
        })

        it("Is number of results equal to 20", () => {
            assert.equal(comics.length, 20);
        })

        it("Is each comic having a title", () => {
            let allComicsHaveATitle = true;
            comics.forEach((comic) => {
                allComicsHaveATitle = allComicsHaveATitle && comic.title != undefined;
            })

            assert.equal(allComicsHaveATitle, true);
        })

        it ("Is there some comics with a description", () => {
            let isThereADescription = false;
            comics.forEach((comic) => {
                isThereADescription = isThereADescription || comic.description != null
            })

            assert.equal(isThereADescription, true);
        })
    })
    
    describe( "Get the character Pocahontas", () =>  {
        var error: Error;
        var character: Character;

        before(async () => {
            try {
                let response = await client.findCharacterByName("Pocahontas");

                if (!response){
                    throw new Error("response is undefined");
                }

                character = response;
            } catch (err) {
                error=err as Error
            }
            
        })

        it("Should throw an error", () => {
            expect(error).to.be.an('Error')
        });

        it(`Error message should be ${ErrorMessages.NO_RESULTS_FOUND}`, () => {
            expect(error.message).equal(`${ErrorMessages.NO_RESULTS_FOUND}`);
        })
    });
    
    describe( "Get an empty character", () =>  {
        var error: Error;
        var character: Character;

        before(async () => {
            try {
                let response = await client.findCharacterByName("");

                if (!response){
                    throw new Error("response is undefined");
                }

                character = response;
            } catch (err) {
                error=err as Error
            }
            
        })

        it("Should throw an error", () => {
            expect(error != undefined).to.be.true;
        });

        it(`Error message should start with ${ErrorMessages.REQUEST_FAILED_WITH_STATUS_CODE}`, () => {
            expect(error.message.startsWith(`${ErrorMessages.REQUEST_FAILED_WITH_STATUS_CODE}`)).to.be.true;
        })
    });
});