const assert = require('assert');
import Character from "../../domain/model/Character";

// Character model test class
describe("Character Test",() => {

    describe( "Create a character",() => {
        let character = new Character(1234, "My character", "An incredible character !");

        it("Is returning 1234 when getting the character's id",() => {
            assert.equal(character.id, 1234);
        });

        it("Is returning 'My character' when getting the character's name",() => {
            assert.equal(character.name, "My character");
        });

        it("Is returning 'An incredible character !' when getting the character's description",() => {
            assert.equal(character.description, "An incredible character !");
        });
    });
});