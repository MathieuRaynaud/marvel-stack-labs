const assert = require('assert');
import Comic from '../../domain/model/Comic'

// Comic model test class
describe("Comic Test",() => {

    describe( "Create a comic",() => {
        let comic = new Comic("My comic", "An incredible comic !");

        it("Is returning 'My comic' when getting the comic's title",() => {
            assert.equal(comic.title, "My comic");
        });

        it("Is returning 'An incredible comic !' when getting the comic's description",() => {
            assert.equal(comic.description, "An incredible comic !");
        });
    });
});