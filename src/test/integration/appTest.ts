import app from '../../app';
import { expect } from "chai";
import { Response } from 'express';
var request = require('supertest');

describe('API Tests', () => {
    it('Should answer with a JSON', function(done) {
        request(app)
        .get('/character?name=deadpool')
        .expect('Content-Type', /json/)
        .expect(200, done)
    });

    it('Should return a response', async function() {
        const response = await request(app).get('/character?name=thor');

        expect(response.status).equal(200);

        expect(response.body.name).equal("Thor");
        expect(response.body.description).to.be.equal("As the Norse God of thunder and lightning, Thor wields one of the greatest weapons ever made, the enchanted hammer Mjolnir. While others have described Thor as an over-muscled, oafish imbecile, he's quite smart and compassionate.  He's self-assured, and he would never, ever stop fighting for a worthwhile cause.");
        expect(response.body.comics).to.exist;
    })

    it('Should return an error 500 when no result is found', function(done) {
        request(app)
        .get('/character?name=tintin')
        .end(function(err: Error, res: Response) {
            expect(res.statusCode).to.equal(500);
            done();
        });
    })

    it('Should return an error 404 when the given URL is bad', function(done) {
        request(app)
        .get('/some-bad-url')
        .end(function(err: Error, res: Response) {
            expect(res.statusCode).to.equal(404);
            done();
        });
    })

    it('Should return an error 500 when the query parameter name is missing', function(done) {
        request(app)
        .get('/character')
        .end(function(err: Error, res: Response) {
            expect(res.statusCode).to.equal(500);
            done();
        });
    })

    it('Should return a response even if both name and other parameters are given to /character', function(done) {
        request(app)
        .get('/character?toto=titi&name=hulk&something=bad')
        .end(function(err: Error, res: Response) {
            expect(res.statusCode).to.equal(200);
            done();
        });
    })
    
});